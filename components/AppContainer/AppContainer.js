
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import{HOME, DETAILS} from '../../routes'

import HomeScreen from '../HomeScreen'
import DetailsScreen from '../Details'

const AppNavigator = createStackNavigator({
    HOME: HomeScreen,
    DETAILS: DetailsScreen,
  },
  {
    initialRouteName: HOME,
  });

  const AppContainer = createAppContainer(AppNavigator);

  export default AppContainer;