import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react'

const styles = StyleSheet.create({
    wrapper:{
        flex: 1,
        margin: 5,
    },
    pic: {
        width: 150,
        height:150,
    },
    h2: {
        textAlign: 'center',
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold',
    }
})

const ImageCard = ({links, title, onPress}) => {
    const {wrapper, pic, h2} = styles
    return(
        <TouchableOpacity onPress={onPress}>
        <View style={wrapper}>
            <Image style={pic} source={{uri:links.thumb}}/>
            <Text style={h2}>{title}</Text>
        </View>
        </TouchableOpacity>
    )
}

export default ImageCard