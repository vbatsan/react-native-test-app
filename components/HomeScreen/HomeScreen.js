import React from "react";
import { ScrollView, Text, StyleSheet, View} from "react-native";

import ImageCard from "../ImageCard";

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    flexDirection:'row',
    padding: 15,
    flexWrap: 'wrap',
    backgroundColor: 'gray'
  },
  img: {
    width:200,
    height:300,
  }
})

export default class HomeScreen extends React.Component {
  state = {
    data:[],
  };
  static navigationOptions = {
    title: "Home"
  };

  componentDidMount() {
    try{
      fetch(
        "https://api.unsplash.com/photos/?client_id=ae16e5ad40f34166daa2fb9715dbdbf327eec498b218897e2484d8d49fc6d5e0"
      )
        .then(resp => resp.json())
        .then(data => this.setState({ data }));
    }
    catch(error) {
      console.log(error)
    }
       
  }

  render() {
    const { data } = this.state;
    const{navigation} = this.props
    const {container, img} = styles
    return (
      <ScrollView >
        <View style={container}>
          {data.map(({user, id, urls}) => {
          return <ImageCard 
            style={img}
            title={user.name} 
            key={id} 
            links={urls}
            onPress={() => navigation.navigate('DETAILS',{link: urls.full, header: user.name})}/>
        })}
        </View>
        
      </ScrollView>
    );
  }
}


