import React from 'react';
import { View, Text, Image, ImageBackground } from 'react-native'

export default class DetailsScreen extends React.Component {
    static  navigationOptions = {
        title: 'Details',
      };
    render() {
        const {state} = this.props.navigation;
        return(
            <ImageBackground style={{ flex:1}} source={require('../../img/Loading.jpg')}>
                <Image source={{uri: state.params.link}} style={{flex:1}}/>
                
            </ImageBackground>
        )
    }
   
}